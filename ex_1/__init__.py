# _*_ coding: utf-8 _*_
"""
Time:     2021/10/23 12:09
Author:   LucifMonX
Version:  V 3.9
File:     __init__.py.py
Describe: 线性回归 例题1.1：假设你是一家餐厅的CEO，正在考虑开一家分店，根据该城市人口数据预测其利润
视频学习地址：https://www.bilibili.com/video/BV1Xt411s7KY?p=1
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

'''
population:人口
profit:利润
'''
path = 'ex1data1.txt'
data = pd.read_csv(path, names=['population', 'profit'])
'''
print(data.head())
   population   profit
0      6.1101  17.5920
1      5.5277   9.1302
2      8.5186  13.6620
3      7.0032  11.8540
4      5.8598   6.8233
print(data.tail())
    population   profit
92      5.8707  7.20290
93      5.3054  1.98690
94      8.2934  0.14454
95     13.3940  9.05510
96      5.4369  0.61705
print(data.describe())
       population     profit
count   97.000000  97.000000
mean     8.159800   5.839135
std      3.869884   5.510262
min      5.026900  -2.680700
25%      5.707700   1.986900
50%      6.589400   4.562300
75%      8.578100   7.046700
max     22.203000  24.147000
'''
# 设置二维散列图
data.plot.scatter('population', 'profit', label='population')
plt.show()
# data.plot(kind='scatter', x='population', y='profit', figsize=(12, 8))
# plt.show()

# 让我们在训练集中添加一列，以便我们可以使用向量化的解决方案来计算代价和梯度
data.insert(0, 'ones', 1)
# 切片 取前两列，取最后一列
X = data.iloc[:, 0:-1]
print(X)
Y = data.iloc[:, -1]
print(Y)

X = X.values
print(X.shape)
y = Y.values
print(y.shape)

y = y.reshape(97, 1)
print(y.shape)

print('-------------------定义损失函数---------------------')


# X:特征值 y:标签 参数：theta
def computeCost(X, y, theta):
    inner = np.power((X @ theta - y), 2)
    return np.sum(inner) / (2 * len(X))


# X的维度是(97,2) y的维度是（97,1） 所以矩阵相乘要乘以一个（2,1）
theta = np.zeros((2, 1))
theta.shape
print(theta.shape)
# 将dataframe转换为matrix矩阵
# X = np.matrix(X)
# y = np.matrix(y)
# theta = np.matrix(np.array([0, 0]))
# theta = np.matrix(np.array([0, 0]))
# # X为（97，2），97行，97个数据，2列，2个特征值，其中一个为常数,theta转置，2行1列，2行即为系数
# error = (X * theta.T) - y
# # 尝试计算下代价函数（即为损失函数的平均值）
cost_init = computeCost(X, y, theta)
print(cost_init)
print('---------------------梯度下降函数-----------------------')


# alpha：学习速度  iters：迭代次数
def gradientDescent(X, y, theta, alpha, iters):
    costs = []
    for i in range(iters):
        # X.T:X的转置
        theta = theta - (X.T @ (X @ theta - y)) * alpha / len(X)
        cost = computeCost(X, y, theta)
        costs.append(cost)

        if i % 100 == 0:
            print(cost)
    return theta, costs


alpha = 0.02
iters = 2000
theta, costs = gradientDescent(X, y, theta, alpha, iters)
# 迭代完成后的目标数值
print(theta)
# 横坐标
x = np.linspace(data.population.min(), data.population.max(), 100)

print('-------------------------可视化损失函数-------------------------')
flg, ax = plt.subplots(2, 3)
# 指定哪个实例
ax1 = ax[0, 0]
# 用来设置属性
ax1.plot
plt.show()
#  fig代表绘图窗口(Figure)；ax代表这个绘图窗口上的坐标系(axis)，一般会继续对ax进行操作
fig, ax = plt.subplots()
ax.plot(np.arange(iters), costs, 'b')
ax.set_xlabel('iters')
ax.set_ylabel('cost')
ax.set_title('cost vs. iters')
plt.show()
print('-------------------------------拟合函数可视化----------------------------')
x = np.linspace(y.min(), y.max(), 100)
y_ = theta[0, 0] + theta[1, 0] * x

fig, ax = plt.subplots()
ax.scatter(X[:, 1], y, label='training data')
ax.plot(x, y_, 'r', label='predict')
ax.legend()
ax.set_xlabel('population')
ax.set_ylabel('profit')
plt.show()
