# _*_ coding: utf-8 _*_
"""
Time:     2021/10/27 21:08
Author:   LucifMonX
Version:  V 3.9
File:     正规方程求解.py
Describe: 利用正规方程求解1.1，对比梯度下降
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

path = 'ex1data1.txt'
data = pd.read_csv(path, names=['population', 'profit'])
data.insert(0, 'ones', 1)
X = data.iloc[:, 0:-1]
y = data.iloc[:, -1]
X = X.values
y = y.values
y = y.reshape(97, 1)


def normalEquation(X, y):
    theta = np.linalg.inv(X.T @ X) @ X.T @ y
    return theta


theta = normalEquation(X, y)
print(theta)