# Machine_Learning

#### Description
吴恩达机器学习课后习题手敲python答案
#### Software Architecture
Software architecture description

#### Installation

1.  python 3.9 
2.  pycharm 2021

#### Instructions

1.  视频笔记（白嫖）：http://www.ai-start.com/ml2014/ 
2.  视频（白嫖）：https://www.bilibili.com/video/BV164411b7dx?from=search&seid=6106850134281731821&spm_id_from=333.337.0.0
3.  ...

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
