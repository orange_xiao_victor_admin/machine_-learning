# _*_ coding: utf-8 _*_
"""
Time:     2021/11/10 20:52
Author:   LucifMonX
Version:  V 3.9
File:     神经网络2.py
Describe: 神经网络解决多分类问题
2.案例：手写数字识别
数据集：ex4data1.mat
初始参数：ex4weights.mat
搭建完整的神经网络
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from scipy.optimize import minimize

print('------------1.常用代码---------------')
path = 'ex4data1.mat'
data = sio.loadmat(path)
raw_X = data['X']
raw_Y = data['y']
X = np.insert(raw_X, 0, values=1, axis=1)
# (5000,401)
print(X.shape)
print('-----------2.对y进行独热编码处理：one-hot编码-------------------')


def one_hot_encoder(raw_y):
    result = []

    for i in raw_y:  # 1-10
        y_temp = np.zeros(10)
        y_temp[i - 1] = 1

        result.append(y_temp)
    return np.array(result)


y = one_hot_encoder(raw_Y)
print(y, y.shape)
# 初始权重参数
theta = sio.loadmat('ex4weights.mat')
theta1, theta2 = theta['Theta1'], theta['Theta2']
# (25, 401) (10, 26)
print(theta1.shape, theta2.shape)

print('---------------3.序列化权重参数-----------------------')


def serialize(a, b):
    # flatten 降维，折叠成一维，默认按行折叠
    return np.append(a.flatten(), b.flatten())


theta_serialize = serialize(theta1, theta2)
# (10285,)  10285=25*401+10*26
print(theta_serialize.shape)

print('---------------4.解序列化权重参数--------------------')


def deserialize(theta_serialize):
    theta1 = theta_serialize[:25 * 401].reshape(25, 401)
    theta2 = theta_serialize[25 * 401:].reshape(10, 26)
    return theta1, theta2


theta1, theta2 = deserialize(theta_serialize)
# (25, 401) (10, 26)
print(theta1.shape, theta2.shape)

print('---------------5.前向传播-------------------------')


# sigmoid函数
def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def feed_forward(theta_serialize, X):
    theta1, theta2 = deserialize(theta_serialize)
    a1 = X
    z2 = a1 @ theta1.T
    a2 = sigmoid(z2)
    a2 = np.insert(a2, 0, values=1, axis=1)
    z3 = a2 @ theta2.T
    h = sigmoid(z3)
    return a1, z2, a2, z3, h


print('---------------6.损失函数(分为有正则化和无正则化)-----------------------')


# 6.1不带正则化的损失函数
def cost(theta_serialize, X, y):
    a1, z2, a2, a3, h = feed_forward(theta_serialize, X)
    J = -np.sum(y * np.log(h) + (1 - y) * np.log(1 - h)) / len(X)
    return J


# 0.2876291651613189
print(cost(theta_serialize, X, y))


# 6.2带正则化的损失函数
def reg_cost(theta_serialize, X, y, lamda):
    sum1 = np.sum(np.power(theta1[:, 1:], 2))
    sum2 = np.sum(np.power(theta2[:, 1:], 2))
    reg = (sum1 + sum2) * lamda / (2 * len(X))
    return reg + cost(theta_serialize, X, y)


# 0.38376985909092365
print(reg_cost(theta_serialize, X, y, 1))

print('-------------7.反向传播-----------------')


# 无正则化的梯度
def sigmoid_gradient(z):
    return sigmoid(z) * (1 - sigmoid(z))


def gradient(theta_serialize, X, y):
    theta1, theta2 = deserialize(theta_serialize)
    a1, z2, a2, z3, h = feed_forward(theta_serialize, X)
    d3 = h - y
    d2 = d3 @ theta2[:, 1:] * sigmoid_gradient(z2)
    D2 = (d3.T @ a2) / len(X)
    D1 = (d2.T @ a1) / len(X)
    return serialize(D1, D2)


# 带正则化的梯度
def reg_gradient(theta_serialize, X, y, lamda):
    D = gradient(theta_serialize, X, y)
    D1, D2 = deserialize(D)
    theta1, theta2 = deserialize(theta_serialize)
    D1[:, 1:] = D1[:, 1:] + theta1[:, 1:] * lamda / len(X)
    # D1[:, 1:] = D1[:, 1:] + theta1[:, 1] * lamda / len(X)
    D2[:, 1:] = D2[:, 1:] + theta2[:, 1:] * lamda / len(X)
    # D2[:, 1:] = D2[:, 1:] + theta2[:, 1] * lamda / len(X)
    return serialize(D1, D2)


print('------------8.神经网络的优化-----------------')
from scipy.optimize import minimize


def nn_training(X, y):
    init_theta = np.random.uniform(-0.5, 0.5, 10285)
    res = minimize(fun=reg_cost, x0=init_theta, args=(X, y, lamda), method='TNC', jac=reg_gradient,
                   options={'maxiter': 300})
    return res


lamda = 10
res = nn_training(X, y)

raw_Y = data['y'].reshape(5000, )

_, _, _, _, h = feed_forward(res.x, X)
y_pred = np.argmax(h, axis=1) + 1
acc = np.mean(y_pred == raw_Y)
# 0.9398
print(acc)

print('---------------9.可视化隐藏层----------------')


def plot_hidden_layer(theta):
    theta1, _ = deserialize(theta)
    hidden_layer = theta1[:, 1:]  # 25,400

    fig, ax = plt.subplots(ncols=5, nrows=5, figsize=(8, 8), sharex=True, sharey=True)

    for r in range(5):
        for c in range(5):
            ax[r, c].imshow(hidden_layer[5 * r + c].reshape(20, 20).T, cmap='gray_r')

    plt.xticks([])
    plt.yticks([])

    plt.show


plot_hidden_layer(res.x)
