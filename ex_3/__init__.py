# _*_ coding: utf-8 _*_
"""
Time:     2021/11/3 21:32
Author:   LucifMonX
Version:  V 3.9
File:     __init__.py.py
Describe: 神经网络
案例：
1.手写数字识别
说明：利用神经网络（前向传播）来实现ex_2里的问题。已经给定了最优的权重参数

"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

path = 'ex3data1.mat'
data = sio.loadmat(path)
raw_X = data['X']
raw_Y = data['y']

X = np.insert(raw_X, 0, values=1, axis=1)
# (5000,401)
print(X.shape)
y = raw_Y.flatten()
# (5000,)
print(y.shape)
# 权重
theta = sio.loadmat('ex3weights.mat')
# dict_keys(['__header__', '__version__', '__globals__', 'Theta1', 'Theta2'])
print(theta.keys())

theta1 = theta['Theta1']
theta2 = theta['Theta2']
# (25, 401) (10, 26)
print(theta1.shape, theta2.shape)


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


a1 = X  # 第一层

z2 = X @ theta1.T
a2 = sigmoid(z2)

print(a2.shape)  # (5000, 25)

a2 = np.insert(a2, 0, values=1, axis=1)
print(a2.shape)  # (5000, 26)

z3 = a2 @ theta2.T
a3 = sigmoid(z3)
print(a3.shape)  # (5000, 10)

# np.argmax 排序索引(max方向)
y_pred = np.argmax(a3, axis=1)
y_pred = y_pred + 1

acc = np.mean(y_pred == y)

print(acc)  # 0.9752
