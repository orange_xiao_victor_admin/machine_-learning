# _*_ coding: utf-8 _*_
"""
Time:     2021/10/24 12:09
Author:   LucifMonX
Version:  V 3.9
File:     __init__.py.py
Describe: 对numpy包的使用
"""
import numpy as np
from numpy import random

arr = np.array([1, 2, 3, 4, 5])

print(arr)  # [1 2 3 4 5]

print(type(arr))  # <class 'numpy.ndarray'>

# 0维数组=
arr0 = np.array(61)

print(arr0)
# 1维数组

arr1 = np.array([1, 2, 3, 4, 5, 6])

print(arr1)
# 2维数组
arr2 = np.array([[1, 2, 3], [4, 5, 6]])

print(arr2)

# 3维数组
arr3 = np.array([[[1, 2, 3], [4, 5, 6]], [[1, 2, 3], [4, 5, 6]]])

print(arr3)

# 输出维数
print(arr0.ndim)
print(arr1.ndim)
print(arr2.ndim)
print(arr3.ndim)

# 数组可以拥有任意数量的维。
# 在创建数组时，可以使用 ndmin 参数定义维数。
arr5 = np.array([1, 2, 3, 4], ndmin=5)

print(arr5)
print('number of dimensions :', arr.ndim)

# 1.从一维数组中获取元素
print(arr1[0])
# 2.访问二维数组
# 2.1 第一维中的第二个元素
print(arr2[0, 1])
# 2.2 第二维中的第三个元素
print(arr2[1, 2])
# 3.访问三维数组
# 3.1 访问第一个数组的第二个数组的第三个元素
print(arr3[0, 1, 2])
# 负索引 打印第二个维中的的最后一个元素：
arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
print('Last element from 2nd dim: ', arr[1, -1])

# 数组裁剪
# 从下面的数组中裁切出元素[1-5]：
arr = np.array([1, 2, 3, 4, 5, 6, 7])
print(arr[0:5])
# 裁切数组[0-last]
print(arr[3:arr.__len__()])
print(arr[3:])
# 负裁剪
# 从末尾开始的索引 3 到末尾开始的索引 1，对数组进行裁切：
print(arr[-3:-1])

# 使用 step 值确定裁切的步长（跳着裁切）
# 从索引 1 到索引 5，返回相隔的元素：
print(arr[1:5:2])
# 返回数组中当步长为2时的元素
print(arr[::2])
# 裁切2维数组
# 从第二个元素开始，对从索引 1 到索引 4（不包括）的元素进行切片
arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
print(arr[1, 1:4])
# 从两个元素中返回索引 2：
arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
print(arr[0:2, 2])
# 从两个元素裁切索引 1 到索引 4（不包括），这将返回一个 2-D 数组：
arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
print(arr[0:2, 1:4])

# 检查数组的数据类型
arr = np.array([1, 2, 3, 4])
print(arr.dtype)
# 获取包含字符串的数组的数据类型：
arr = np.array(['apple', 'banana', 'cherry'])
print(arr.dtype)
# 用数据类型字符串创建数组：
arr = np.array([1, 2, 3, 4], dtype='S')
print(arr)
print(arr.dtype)
# 创建数据类型为 4 字节整数的数组：
arr = np.array([1, 2, 3, 4], dtype='i4')
print(arr)
print(arr.dtype)
# 转换已有数组的数据类型
'''
更改现有数组的数据类型的最佳方法，是使用 astype() 方法复制该数组。
astype() 函数创建数组的副本，并允许您将数据类型指定为参数。
数据类型可以使用字符串指定，例如 'f' 表示浮点数，'i' 表示整数等。或者您也可以直接使用数据类型，例如 float 表示浮点数，int 表示整数。
'''
# 通过使用 'i' 作为参数值，将数据类型从浮点数更改为整数：
arr = np.array([1.1, 2.1, 3.1])
newarr = arr.astype('i')
print(newarr)
print(newarr.dtype)
# 通过使用 int 作为参数值，将数据类型从浮点数更改为整数：
arr = np.array([1.1, 2.1, 3.1])
newarr = arr.astype(int)
print(newarr)
print(newarr.dtype)
# 将数据类型从整数更改为布尔值：
arr = np.array([1, 0, 3])
newarr = arr.astype(bool)
print(newarr)
print(newarr.dtype)

'''
副本和视图的区别
副本和数组视图之间的主要区别在于副本是一个新数组，而这个视图只是原始数组的视图。
副本拥有数据，对副本所做的任何更改都不会影响原始数组，对原始数组所做的任何更改也不会影响副本。
视图不拥有数据，对视图所做的任何更改都会影响原始数组，而对原始数组所做的任何更改都会影响视图。
'''
# 进行复制从而创建副本，更改原始数组并显示两个数组：
arr = np.array([1, 2, 3, 4, 5])
x = arr.copy()
arr[0] = 61
print(arr)
print(x)
# 创建视图，更改原始数组，然后显示两个数组：
arr = np.array([1, 2, 3, 4, 5])
x = arr.view()
arr[0] = 61
print(arr)
print(x)

# 检查数组是否有数据
'''
如上所述，副本拥有数据，而视图不拥有数据，但是我们如何检查呢？
每个 NumPy 数组都有一个属性 base，如果该数组拥有数据，则这个 base 属性返回 None。
否则，base 属性将引用原始对象。
'''
arr = np.array([1, 2, 3, 4, 5])
x = arr.copy()
y = arr.view()
# 副本返回 None。
# 视图返回原始数组。
print(x.base)
print(y.base)

# numpy数组形状
# 获取数组的形状
arr = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
print(arr.shape)  # 2*4 2行4列矩阵 该数组有 2 个维，每个维有 4 个元素。
# 利用 ndmin 使用值 1,2,3,4 的向量创建有 5 个维度的数组，并验证最后一个维度的值为 4：
arr = np.array([1, 2, 3, 4], ndmin=5)
print(arr)
print('shape of array :', arr.shape)  # 每个索引处的整数表明相应维度拥有的元素数量。索引 4，我们的值为 4，因此可以说第 5 个 ( 4 + 1 th) 维度有 4 个元素。

# 数组重塑
# 将以下具有 12 个元素的 1-D 数组转换为 2-D 数组。最外面的维度将有 4 个数组，每个数组包含 3 个元素：
arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
new_arr = arr.reshape(4, 3)
print(new_arr)
# 将以下具有 12 个元素的 1-D 数组转换为 3-D 数组。最外面的维度将具有 2 个数组，其中包含 3 个数组，每个数组包含 2 个元素：
arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
new_arr = arr.reshape(2, 3, 2)
print(new_arr)
# 检查返回的数组是副本还是视图： （返回原始数组，所以是个视图）
print(new_arr.base)
# 未知的维
'''
这意味着您不必在 reshape 方法中为维度之一指定确切的数字。
传递 -1 作为值，NumPy 将为您计算该数字。
'''
# 将 8 个元素的 1D 数组转换为 2x2 元素的 3D 数组：
arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
new_arr = arr.reshape(2, 2, -1)
print(new_arr)
# 展平数组:将多维数组转换为 1D 数组。
arr = np.array([[1, 2, 3], [4, 5, 6]])
new_arr = arr.reshape(-1)
print(new_arr)

# 迭代1-D数组
arr = np.array([1, 2, 3])
for x in arr:
    print(x)
# 迭代2-D数组
arr = np.array([[1, 2, 3], [4, 5, 6]])
for x in arr:  # 循环一次输出的1-D数组
    print(x)
print('-----------------------------------------')
for x in arr:
    for y in x:
        print(y)
print('-----------------------------------------')
# 迭代3-D数组
arr = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])
for x in arr:
    print(x)

# 使用 nditer() 迭代数组.
# 背景：在基本的 for 循环中，迭代遍历数组的每个标量，我们需要使用 n 个 for 循环，对于具有高维数的数组可能很难编写。这个时候可以用辅助函数nditer()
# 遍历一个 3-D 数组：
arr = np.array([[[1, 2], [3, 4]], [[5, 6], [7, 8]]])
for x in np.nditer(arr):
    print(x)
# 遍历一个2-D数组
arr = np.array([[1, 2, 3], [4, 5, 6]])
for x in np.nditer(arr):
    print(x)
print('-----------------------------------------')
# 迭代不同数据类型的数组
'''
我们可以使用 op_dtypes 参数，并传递期望的数据类型，以在迭代时更改元素的数据类型。
NumPy 不会就地更改元素的数据类型（元素位于数组中），因此它需要一些其他空间来执行此操作，该额外空间称为 buffer，为了在 nditer() 中启用它，我们传参 flags=['buffered']。
'''
arr = np.array([1, 2, 3])
for x in np.nditer(arr, flags=['buffered'], op_dtypes=[str]):
    print(x)
print('-----------------------------------------')
# 以不同的步长迭代，例题：每遍历 2D 数组的一个标量元素，跳过 1 个元素：
arr = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
for x in np.nditer(arr[:, ::2]):
    print(x)
print('-----------------------------------------')
# 使用 ndenumerate() 进行枚举迭代
'''
枚举是指逐一提及事物的序号。
有时，我们在迭代时需要元素的相应索引，对于这些用例，可以使用 ndenumerate() 方法。
'''
# 枚举以下1-D数组元素
arr = np.array([1, 2, 3])
for idx, x in np.ndenumerate(arr):
    print(idx, x)
# 枚举以下2-D数组元素
print('-----------------------------------------')
arr = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
for idx, x in np.ndenumerate(arr):
    print(idx, x)
print('-----------------------------------------')

# 连接Numpy数组
'''
连接意味着将两个或多个数组的内容放在单个数组中。
在 SQL 中，我们基于键来连接表，而在 NumPy 中，我们按轴连接数组。
我们传递了一系列要与轴一起连接到 concatenate() 函数的数组。如果未显式传递轴，则将其视为 0。
'''
# 连接两个1-D数组
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])
arr = np.concatenate((arr1, arr2))
print(arr)
print('-----------------------------------')
# 沿着行 (axis=1) 连接两个 2-D 数组：
'''[[1 2 5 6]
 [3 4 7 8]]'''
arr1 = np.array([[1, 2], [3, 4]])
arr2 = np.array([[5, 6], [7, 8]])
arr = np.concatenate((arr1, arr2), axis=1)
print(arr)
print('-----------------------------------')
# 使用堆栈函数连接数组
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])
arr = np.stack((arr1, arr2), axis=1)
print(arr)
# 沿行堆叠
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])
arr = np.hstack((arr1, arr2))
print(arr)
# 沿列堆叠
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])
arr = np.vstack((arr1, arr2))
print(arr)
# 沿高度堆叠（深度）
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])
arr = np.dstack((arr1, arr2))
print(arr)

# 数组拆分
# 将数组分成三部分
arr = np.array([1, 2, 3, 4, 5, 6])
'''
返回值是一个包含三个数组的数组。
如果数组中的元素少于要求的数量，它将从末尾进行相应调整。
'''
new_arr = np.array_split(arr, 3)
print(new_arr)
# 将数组分成四部分
arr = np.array([1, 2, 3, 4, 5, 6])
new_arr = np.array_split(arr, 4)
print(new_arr)
print('------------------------------------------------------')
# 访问拆分后的数组
print(new_arr[0])
print('------------------------------------------------------')
# 拆分2-D数组
# 把这个 2-D 拆分为三个 2-D 数组。
arr = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11, 12]])
new_arr = np.array_split(arr, 3)
print(new_arr)
print('------------------------------------------------------')
# 把这个 2-D 拆分为三个 2-D 数组。
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [16, 17, 18]])
new_arr = np.array_split(arr, 3)
print(new_arr)
print('------------------------------------------------------')
# 可以指定要进行拆分的轴。下面的例子还返回三个 2-D 数组，但它们沿行 (axis=1) 分割。
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [16, 17, 18]])
new_arr = np.array_split(arr, 3, axis=1)
print(new_arr)
# 另一种解决方案是使用与 hstack() 相反的 hsplit()。使用 hsplit() 方法将 2-D 数组沿着行分成三个 2-D 数组。
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [16, 17, 18]])
new_arr = np.hsplit(arr, 3)
print(new_arr)

print('--------------Numpy 数组搜索-------------------------------')
# 查找值为 4 的索引：
arr = np.array([1, 2, 3, 4, 5, 4, 4])
x = np.where(arr == 4)
print(x)
print('---------------------------------------------------------')
# 查找值为偶数的索引：
arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
x = np.where(arr % 2 == 0)
print(x)  # 输出的是坐标
print("--------------------------------------------------------")
# 查找值为奇数的索引：
arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
x = np.where(arr % 2 == 1)
print(x)
print("------------------------搜索排序--------------------------")
'''
有一个名为 searchsorted() 的方法，该方法在数组中执行二进制搜索，并返回将在其中插入指定值以维持搜索顺序的索引。
假定 searchsorted() 方法用于排序数组。
'''
# 查找应在其中插入值 7 的索引.例子解释：应该在索引 1 上插入数字 7，以保持排序顺序。
arr = np.array([6, 7, 8, 9])
x = np.searchsorted(arr, 7)
print(x)
print(arr)
print('--------------------------------------------------------')
# 从右侧开始搜索。默认情况下，返回最左边的索引，但是我们可以给定 side='right'，以返回最右边的索引。
# 从右边开始查找应该插入值 7 的索引：应该在索引 2 上插入数字 7，以保持排序顺序。
arr = np.array([6, 7, 8, 9])
x = np.searchsorted(arr, 7, side='right')
print(x)
# 多个值,要搜索多个值，请使用拥有指定值的数组。
# 查找应在其中插入值 2、4 和 6 的索引：返回值是一个数组：[1 2 3] 包含三个索引，其中将在原始数组中插入 2、4、6 以维持顺序。
arr = np.array([1, 3, 5, 7])
x = np.searchsorted(arr, [2, 4, 6])
print(x)

print('--------------------------Numpy数组排序------------------------------')
'''
排序是指将元素按有序顺序排列。
有序序列是拥有与元素相对应的顺序的任何序列，例如数字或字母、升序或降序。
NumPy ndarray 对象有一个名为 sort() 的函数，该函数将对指定的数组进行排序。
'''
# 对数组进行排序。注释：此方法返回数组的副本，而原始数组保持不变。
arr = np.array([3, 2, 0, 1])
print(np.sort(arr))
# 对数组以字母顺序进行排序：
arr = np.array(['banana', 'cherry', 'apple'])
print(np.sort(arr))
# 对布尔数组进行排序：
arr = np.array([True, False, True])
print(np.sort(arr))
# 对 2-D 数组排序
arr = np.array([[3, 2, 4], [5, 0, 1]])
print(np.sort(arr))

print('----------------------NumPy 数组过滤----------------------------')
'''
从现有数组中取出一些元素并从中创建新数组称为过滤（filtering）。
在 NumPy 中，我们使用布尔索引列表来过滤数组。
布尔索引列表是与数组中的索引相对应的布尔值列表。
如果索引处的值为 True，则该元素包含在过滤后的数组中；如果索引处的值为 False，则该元素将从过滤后的数组中排除
'''
# 用索引 0 和 2、4 上的元素创建一个数组。返回 [61, 63, 65]
arr = np.array([61, 62, 63, 64, 65])
x = [True, False, True, False, True]
new_arr = arr[x]
print(new_arr)
print('----------------------------创建过滤器数组------------------------------')
# 1.创建一个仅返回大于 62 的值的过滤器数组：

arr = np.array([61, 62, 63, 64, 65])
# 创建一个空列表
filter_arr = []
# 遍历 arr 中的每个元素
for element in arr:
    # 如果元素大于 62，则将值设置为 True，否则为 False：
    if element > 62:
        filter_arr.append(True)
    else:
        filter_arr.append(False)

new_arr = arr[filter_arr]

print(filter_arr)
print(new_arr)

# 2.创建一个过滤器数组，该数组仅返回原始数组中的偶数元素：
arr = np.array([1, 2, 3, 4, 5, 6, 7])
# 创建一个空列表
filter_arr = []
# 遍历 arr 中的每个元素
for element in arr:
    # 如果元素可以被 2 整除，则将值设置为 True，否则设置为 False
    if element % 2 == 0:
        filter_arr.append(True)
    else:
        filter_arr.append(False)

new_arr = arr[filter_arr]

print(filter_arr)
print(new_arr)
print('---------------------直接从数组创建过滤器--------------------')
# 1.创建一个仅返回大于 62 的值的过滤器数组：
arr = np.array([61, 62, 63, 64, 65])
filter_arr = arr > 62
new_arr = arr[filter_arr]
print(filter_arr)
print(new_arr)
# 2.创建一个过滤器数组，该数组仅返回原始数组中的偶数元素：
arr = np.array([1, 2, 3, 4, 5, 6, 7])
filter_arr = arr % 2 == 0
new_arr = arr[filter_arr]
print(filter_arr)
print(new_arr)

print('---------------------------随机数-----------------------------')
# 生成随机数。实例：生成一个 0 到 100 之间的随机整数：
x = random.randint(100)
print(x)
# 生成随机浮点。random 模块的 rand() 方法返回 0 到 1 之间的随机浮点数。
# 生成一个 0 到 100 之间的随机浮点数：
x = random.rand() * 100
print(int(x))
print('--------------------------生成随机数组----------------------------------')
'''
1.整数  randint() 方法接受 size 参数，您可以在其中指定数组的形状。
'''
# 生成一个 1-D 数组，其中包含 5 个从 0 到 100 之间的随机整数：
x = random.randint(100, size=5)
print(x)
# 生成有 3 行的 2-D 数组，每行包含 5 个从 0 到 100 之间的随机整数：
x = random.randint(100, size=(3, 5))
print(x)
'''
2.浮点数  rand() 方法还允许您指定数组的形状。
'''
# 生成包含 5 个随机浮点数的 1-D 数组：
x = random.rand(5)
print(x)
# 生成有 3 行的 2-D 数组，每行包含 5 个随机数：
x = random.rand(3, 5)
print(x)
'''
3. 从数组中生成随机数  
choice() 方法使您可以基于值数组生成随机值。
choice() 方法将数组作为参数，并随机返回其中一个值。
'''
# 返回数组中的值之一：
x = random.choice([3, 5, 7, 9])
print(x)
# choice() 方法还允许您返回一个值数组。请添加一个 size 参数以指定数组的形状。
# 生成由数组参数（3、5、7 和 9）中的值组成的二维数组：
x = random.choice([3, 5, 7, 9], size=(3, 5))
print(x)
print('-----------------------NumPy ufuncs------------------------')
'''
ufuncs 指的是“通用函数”（Universal Functions），它们是对 ndarray 对象进行操作的 NumPy 函数。
对两个列表的元素进行相加：
list 1: [1, 2, 3, 4]
list 2: [4, 5, 6, 7]
一种方法是遍历两个列表，然后对每个元素求和。
'''
# 如果没有 ufunc，我们可以使用 Python 的内置 zip() 方法：
x = [1, 2, 3, 4]
y = [4, 5, 6, 7]
z = []
for i, j in zip(x, y):
    z.append(i + j)
print(z)
# 对此，NumPy 有一个 ufunc，名为 add(x, y)，它会输出相同的结果
z = np.add(x, y)
print(z)
