# _*_ coding: utf-8 _*_
"""
Time:     2021/10/27 21:16
Author:   LucifMonX
Version:  V 3.9
File:     __init__.py.py
Describe: 逻辑回归
线性可分
案例：根据学生的两门成绩，预测学生是否会被大学录取
数据集：ex2data1.txt
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as opt

print('--------------------------1.读取数组---------------------------------')
path = 'ex2data1.txt'
data = pd.read_csv(path, names=['Exam1', 'Exam2', 'Accepted'])
print(data.head())

print('--------------------------2.数据可视化--------------------------------')
fig, ax = plt.subplots()
ax.scatter(data[data['Accepted'] == 0]['Exam1'], data[data['Accepted'] == 0]['Exam2'], c='r', marker='x', label='y=0')
ax.scatter(data[data['Accepted'] == 1]['Exam1'], data[data['Accepted'] == 1]['Exam2'], c='b', marker='o', label='y=1')
ax.legend()
ax.set_xlabel('exam1')
ax.set_ylabel('exam2')

plt.show()



def get_Xy(data):
    # 在第一列插入1
    data.insert(0, 'ones', 1)
    # 取除最后一列以外的列
    X_ = data.iloc[:, 0:-1]
    # 取特征值
    X = X_.values
    # 取最后一列
    y_ = data.iloc[:, -1]
    y = y_.values.reshape(len(y_), 1)
    return X, y


X, y = get_Xy(data)
print(X)
print(y)
# (100,3)
print(X.shape)
# （100，1）
print(y.shape)

print('-----------------------3.损失函数--------------------------------')


# 实现sigmoid函数
def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def Cost_Function(X, y, theta):
    A = sigmoid(X @ theta)
    first = y * np.log(A)
    second = (1 - y) * np.log(1 - A)
    return -np.sum(first + second) / len(X)


theta = np.zeros((3, 1))
print(theta.shape)
cost_init = Cost_Function(X, y, theta)
print(cost_init)

print('---------------------4.梯度下降------------------------------------')


def gradientDescent(X, y, theta, alpha, iters):
    m = len(X)
    costs = []
    for i in range(iters):
        A = sigmoid(X @ theta)
        # X.T:X的转置
        theta = theta - (alpha / m) * X.T @ (A - y)
        cost = Cost_Function(X, y, theta)
        costs.append(cost)
        # if i % 1000 == 0:
        #     print(cost)
    return costs, theta


alpha = 0.004
iters = 200000
costs, theta_final = gradientDescent(X, y, theta, alpha, iters)
print(theta_final)

print('----------------5.预测--------------------')


def predict(X, theta):
    prob = sigmoid(X @ theta)
    return [1 if x >= 0.5 else 0 for x in prob]


print(predict(X, theta_final))

y_ = np.array(predict(X, theta_final))
print(y_)
y_pre = y_.reshape(len(y_), 1)
# 求取均值
acc = np.mean(y_pre == y)
print(acc)
print('-----------------------6.决策边界-------------------------------------')
# 决策边界就是Xθ=0的时候
coef1 = - theta_final[0, 0] / theta_final[2, 0]
coef2 = - theta_final[1, 0] / theta_final[2, 0]
x = np.linspace(20, 100, 100)
f = coef1 + coef2 * x
fig, ax = plt.subplots()
ax.scatter(data[data['Accepted'] == 0]['Exam1'], data[data['Accepted'] == 0]['Exam2'], c='r', marker='x', label='y=0')
ax.scatter(data[data['Accepted'] == 1]['Exam1'], data[data['Accepted'] == 1]['Exam2'], c='b', marker='o', label='y=1')
ax.legend()
ax.set_xlabel('exam1')
ax.set_ylabel('exam2')
ax.plot(x, f, c='g')
plt.show()
