# _*_ coding: utf-8 _*_
"""
Time:     2021/10/31 21:33
Author:   LucifMonX
Version:  V 3.9
File:     解决多分类问题.py
Describe: 逻辑回归解决多分类问题
案例：手写数字识别
"""
'''
以多项式来解释，x的次数越高，预测能力越差。高次项会导致过拟合问题的产生。
发生过拟合问题时：
1.丢弃一些不能帮助我们正确预测的特征。可以是手工选择保留哪些特征，或者使用一些模型选择的算法来帮忙（例如PCA）
2.正则化。 保留所有的特征，但是减少参数的大小（magnitude）。
'''
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

# 读入数据
path = 'ex3data1.mat'
data = sio.loadmat(path)
print(data)
print(type(data))
print(data.keys())
raw_X = data['X']
raw_Y = data['y']
# (5000, 400)
print(raw_X.shape)
# (5000, 1)
print(raw_Y.shape)


def plot_an_image(X):
    pick_one = np.random.randint(5000)
    image = X[pick_one, :]
    fig, ax = plt.subplots(figsize=(1, 1))
    ax.imshow(image.reshape(20, 20).T, cmap='gray_r')
    plt.xticks([])
    plt.yticks([])


plot_an_image(raw_X)
plt.show()


def plot_100_images(X):
    sample_index = np.random.choice(len(X), 100)
    images = X[sample_index, :]
    print(images.shape)

    fig, ax = plt.subplots(ncols=10, nrows=10, figsize=(8, 8), sharex=True, sharey=True)

    for r in range(10):
        for c in range(10):
            ax[r, c].imshow(images[10 * r + c].reshape(20, 20).T, cmap='gray_r')

    plt.xticks([])
    plt.yticks([])
    plt.show()


plot_100_images(raw_X)
plt.show()


# 损失函数，找出最小的损失函数
def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def Cost_Function(theta, X, y, lamda):
    A = sigmoid(X @ theta)
    first = y * np.log(A)
    second = (1 - y) * np.log(1 - A)
    reg = np.sum(np.power(theta[1:], 2)) * (lamda / (2 * len(X)))
    return -np.sum(first + second) / len(X) + reg


def gradient_reg(theta, X, y, lamda):
    reg = theta[1:] * (lamda / len(X))
    reg = np.insert(reg, 0, values=0, axis=0)
    first = (X.T @ (sigmoid(X @ theta) - y)) / len(X)
    return first + reg


X = np.insert(raw_X, 0, values=1, axis=1)
# (5000, 401)
print(X.shape)
y = raw_Y.flatten()
# (5000,)
print(y.shape)

# 利用内置函数求最优化
from scipy.optimize import minimize


# K为标签个数
def one_vs_all(X, y, lamda, K):
    n = X.shape[1]
    theta_all = np.zeros((K, n))

    for i in range(1, K + 1):
        theta_i = np.zeros(n, )

        res = minimize(fun=Cost_Function,
                       x0=theta_i,
                       args=(X, y == i, lamda),
                       method='TNC',
                       jac=gradient_reg
                       )
        theta_all[i - 1, :] = res.x
    return theta_all


lamda = 1
K = 10
theta_final = one_vs_all(X, y, lamda, K)
print(theta_final)


def predict(X, theta_final):
    # (5000,401) (10,401) => (5000,10)
    h = sigmoid(X @ theta_final.T)
    h_argmax = np.argmax(h, axis=1)
    return h_argmax + 1


y_pred = predict(X, theta_final)
acc = np.mean(y_pred == y)
# 0.9446
print(acc)
